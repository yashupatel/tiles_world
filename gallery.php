<?php
include('include/header.php');
?>

<!-- banner 2 -->
<div class="banner2-w3ls">

</div>
<!-- //banner 2 -->
</div>
<!-- main -->
<!-- page details -->
<div class="breadcrumb-agile">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item">
                <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Gallery</li>
        </ol>
    </nav>
</div>
<!-- //page details -->

<!-- gallery page -->
<div class="gallery py-5">
    <div class="container py-xl-5 py-lg-3">
        <div class="title text-center mb-5">
            <h2 class="text-dark mb-2">Gallery</h2>
            
        </div>
        <div class="row w3ls_portfolio_grids">
            <div class="col-sm-4 agileinfo_portfolio_grid">
                <div class="w3_agile_portfolio_grid1">
                    <a href="images/n2.jpg" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Tiles World</h3>
                            </div>
                            <img src="images/n2.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
                <div class="w3_agile_portfolio_grid1">
                    <a href="images/n3.jpg" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Tiles World</h3>
                            </div>
                            <img src="images/n3.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
                <div class="w3_agile_portfolio_grid1">
                    <a href="images/n4.jpg" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Tiles World</h3>
                            </div>
                            <img src="images/n4.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-4 agileinfo_portfolio_grid">
                <div class="w3_agile_portfolio_grid1">
                    <a href="images/n5.jpg" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Tiles World</h3>
                            </div>
                            <img src="images/n5.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
                <div class="w3_agile_portfolio_grid1">
                    <a href="images/n7.jpg" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Tiles World</h3>
                            </div>
                            <img src="images/n7.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
                <div class="w3_agile_portfolio_grid1">
                    <a href="images/n8.jpg" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Tiles World</h3>
                            </div>
                            <img src="images/n8.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-4 agileinfo_portfolio_grid">
                <div class="w3_agile_portfolio_grid1">
                    <a href="images/n9.jpg" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Tiles World</h3>
                            </div>
                            <img src="images/n9.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
                <div class="w3_agile_portfolio_grid1">
                    <a href="images/n1.jpg" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Tiles World</h3>
                            </div>
                            <img src="images/n1.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
                <div class="w3_agile_portfolio_grid1">
                    <a href="images/n6.jpg" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Tiles World</h3>
                            </div>
                            <img src="images/n6.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //gallery page -->


<?php
include('include/footer.php');
?>