<?php
include('include/header.php');
?>

<!-- banner 2 -->
<div class="banner2-w3ls">

</div>
<!-- //banner 2 -->
</div>
<!-- main -->
<!-- page details -->
<div class="breadcrumb-agile">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item">
                <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
        </ol>
    </nav>
</div>
<!-- //page details -->

<!-- contact page -->
<div class="address py-5">
    <div class="container py-xl-5 py-lg-3">
        <div class="title text-center mb-5">
            <h2 class="text-dark mb-2">Contact Us</h2>

        </div>
        <div class="row address-row">
            <div class="col-lg-6 address-right">
                <div class="address-info wow fadeInRight animated" data-wow-delay=".5s">
                    <h4 class="font-weight-bold mb-3">Address</h4>
                    <p>A-26,Sara Nagar,Opp.Yash Complex,30 mt Road,Gotri,Vadodara - 390 021</p>
                </div>
                <div class="address-info address-mdl wow fadeInRight animated" data-wow-delay=".7s">
                    <h4 class="font-weight-bold mb-3">Phone </h4>
                    <a href="tel:+91 9687523723">
                        <p>+91 7622014922</p>
                    </a>
                    <a href="tel:+918849110325">
                        <p>+91 8849110325</p>
                    </a>
                    <a href="tel:+919327614922">
                        <p>+91 9327614922</p>
                    </a>
                </div>
                <div class="address-info agileits-info wow fadeInRight animated" data-wow-delay=".6s">
                    <h4 class="font-weight-bold mb-3">Mail</h4>
                    <p>
                        <a href="mailto:tilesworldvadodara@gmail.com">tilesworldvadodara@gmail.com</a>
                    </p>
                </div>
            </div>
            <div class="col-lg-6 address-left mt-lg-0 mt-5">
                <div class="address-grid">
                    <h4 class="font-weight-bold mb-3">Get In Touch</h4>
                    <form action="#" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name" name="name" required="">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email" name="email" required="">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Subject" name="subject" required="">
                        </div>
                        <div class="form-group">
                            <textarea placeholder="Message" class="form-control" required=""></textarea>
                        </div>
                        <input type="submit" value="SEND">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- map -->
<div class="map">
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14763.940438129022!2d73.137525!3d22.3164029!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1d85ce0ffbe35872!2sTiles%20World!5e0!3m2!1sen!2sin!4v1595497849947!5m2!1sen!2sin"
        width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
        tabindex="0"></iframe>
</div>
<!--// map -->
<!-- //contact page -->


<?php
include('include/footer.php');
?>