	<!-- footer -->
	<footer class="text-center py-sm-4 py-3">
	    <div class="container py-xl-5 py-3">
	        <div class="w3l-footer footer-social-agile mb-4">
	            <ul class="list-unstyled">
	                <li>
	                    <a href="https://www.facebook.com/tilesworldvadodara/" target="_blank" >
	                        <i class="fab fa-facebook-f"></i>
	                    </a>
					</li>
	                <li class="mx-1">
	                    <a href="javascript:void(0);">
	                        <i class="fab fa-twitter"></i>
	                    </a>
	                </li>
	                <li>
	                    <a href="javascript:void(0);">
	                        <i class="fab fa-instagram"></i>
	                    </a>
	                </li>
	            </ul>
	        </div>
	        <!-- copyright -->
	        <p class="copy-right-grids text-light my-lg-5 my-4 pb-4">© 2018 Tiles World. All Rights Reserved | Design by
	            <b> <a href="http://www.4foxwebsolution.com/" target="_blank">4 FOX WEB SOLUTION</a>
	            </b>
	        </p>
	        <!-- //copyright -->
	    </div>
	    <div id="WAButton"></div>
	    <!-- chef -->
	    <!-- <img src="images/chef.png" alt="" class="img-fluid chef-style" /> -->
	    <!-- //chef -->
	</footer>
	<!-- //footer -->


	<!-- Js files -->
	<!-- JavaScript -->

	<script>
$(function() {
    $('#WAButton').floatingWhatsApp({
        phone: '+91 9327614922', //WhatsApp Business phone number International format-
        //Get it with Toky at https://toky.co/en/features/whatsapp.
        headerTitle: 'Chat with us on WhatsApp!', //Popup Title
        popupMessage: 'Hello!!! How can we help you?', //Popup Message
        showPopup: true, //Enables popup display
        buttonImage: '<img src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/whatsapp.svg" />', //Button Image
        //headerColor: 'crimson', //Custom header color
        //backgroundColor: 'crimson', //Custom background button color
        position: "right",
        size: "50px"
    });
});
$(window).load(function() {
    $("#flexiselDemo1").flexisel({
        visibleItems: 1,
        animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 3000,
        pauseOnHover: true,
        enableResponsiveBreakpoints: true,
        // responsiveBreakpoints: {
        // 	portrait: {
        // 		changePoint: 480,
        // 		visibleItems: 1
        // 	},
        // 	landscape: {
        // 		changePoint: 640,
        // 		visibleItems: 2
        // 	},
        // 	tablet: {
        // 		changePoint: 768,
        // 		visibleItems: 2
        // 	}
        // }
    });

});
	</script>
	<!-- //flexisel (for special offers) -->

	<!-- script for tabs -->
	<script>
$(function() {

    var menu_ul = $('.faq > li > ul'),
        menu_a = $('.faq > li > a');

    menu_ul.hide();

    menu_a.click(function(e) {
        e.preventDefault();
        if (!$(this).hasClass('active')) {
            menu_a.removeClass('active');
            menu_ul.filter(':visible').slideUp('normal');
            $(this).addClass('active').next().stop(true, true).slideDown('normal');
        } else {
            $(this).removeClass('active');
            $(this).next().stop(true, true).slideUp('normal');
        }
    });

});
	</script>
	<!-- script for tabs -->

	<!-- stats -->

	<script>
$('.counter').countUp();
	</script>
	<!-- //stats -->

	<!-- menu-js -->
	<script>
$('.navicon').on('click', function(e) {
    e.preventDefault();
    $(this).toggleClass('navicon--active');
    $('.toggle').toggleClass('toggle--active');
});
	</script>
	<!-- //menu-js -->



	</body>

	</html>