<?php
include('include/header.php');
?>

<!-- banner 2 -->
<div class="banner2-w3ls">

</div>
<!-- //banner 2 -->
</div>
<!-- main -->
<!-- page details -->
<div class="breadcrumb-agile">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item">
                <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Bathware</li>
        </ol>
    </nav>
</div>
<!-- //page details -->

<!-- gallery page -->
<div class="gallery py-5">
    <div class="container py-xl-5 py-lg-3">
        <div class="title text-center mb-5">
            <b>
                <h2 class="text-dark mb-2">Bathware</h2>
            </b>

        </div>
        <div class="row w3ls_portfolio_grids">
            <div class="col-sm-4 agileinfo_portfolio_grid">
                <div class="w3_agile_portfolio_grid1">
                    <a href="javascript:void(0);" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Bath Ware</h3>
                            </div>
                            <img src="images/bath tubs 2.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-4 agileinfo_portfolio_grid">
                <div class="w3_agile_portfolio_grid1">
                    <a href="javascript:void(0);" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Bath Ware</h3>
                            </div>
                            <img src="images/bath ware 6.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-4 agileinfo_portfolio_grid">
                <div class="w3_agile_portfolio_grid1">
                    <a href="javascript:void(0);" title="Tiles World">
                        <div class="agileits_portfolio_sub_grid agileits_w3layouts_team_grid">
                            <div class="w3layouts_port_head">
                                <h3>Bath Ware</h3>
                            </div>
                            <img src="images/bath ware 8.jpg" alt=" " class="img-fluid" />
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //gallery page -->


<?php
include('include/footer.php');
?>