<?php
include('include/header.php');
?>
<!-- banner -->
<!-- main image slider container -->
<div id="slider-main">
    <div class="banner-text-agile text-center">
        <div class="container">
            <h3 class="text-white font-weight-bold mb-3"> <br><br><br><br> Welcom to Tiles World</h3>
            <p class="text-light">We are here for best services and clint satisfaction.</p>
            <!-- <button type="button" class="btn btn-primary button mt-md-5 mt-4" data-toggle="modal"
                data-target=".bd-example-modal-lg">
                <span>Watch Our Video</span>
            </button> -->
        </div>
    </div>
    <!-- previous button -->
    <button id="prev">
        <i class="fas fa-chevron-left"></i>
    </button>

    <!-- image container -->
    <div id="slider"></div>

    <!-- next button -->
    <button id="next">
        <i class="fas fa-chevron-right"></i>
    </button>

    <!-- small circles container -->
    <div id="circles" style="margin-bottom: 30px;"></div>
</div>
<!-- end of main image slider container -->
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="exampleModalCenterTitle">Cake Bakery</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0">
                <iframe src="https://player.vimeo.com/video/58582025" style="border:none"></iframe>
            </div>
        </div>
    </div>
</div>
<!-- //Model -->
<!-- //banner -->

<!-- banner bottom icons -->
<div class="icons-banner-botom">
    <div class="container">
        <center>
            <ul class="list-unstyled my-4">

                <li class="icons-mkw3ls">
                    <p class="mb-3">Tiles</p>
                    <img src="images/n88.jpg" class="img-fluid" alt="">
                </li>
                <li class="icons-mkw3ls">
                    <p class="mb-3">Kitchen</p>
                    <img src="images/k4.jpeg" class="img-fluid" alt="">
                </li>
                <li class="icons-mkw3ls">
                    <p class="mb-3">Bathware</p>
                    <img src="images/bath tubs 2.jpg" class="img-fluid" alt="">
                </li>
                <li class="icons-mkw3ls">
                    <p class="mb-3">Sanitaryware</p>
                    <img src="images/s1.jpg" class="img-fluid" alt="">
                </li>



            </ul>
        </center>
    </div>
</div>
<!-- //banner bottom icons -->
</div>
<!-- //main -->

<!-- banner-bottom -->
<section class="banner-main-agiles py-5">
    <div class="banner-bottom-w3layouts" id="about">
        <div class="container pt-xl-5 pt-lg-3">
            <div class="row mt-5">
                <div class="col-lg-6">
                    <p class="text-uppercase">A few words</p>
                    <h3 class="aboutright">We Provide best qulity Products.</h3>
                    <h4 class="aboutright">We Provide you with wide range of products with affordable price.</h4>
                    <p>We are leading wholesale of ceramic tiles with variety of product and affordable prices for
                        coustomer and with best quality
                        assurance, coustomer satisfaction. our motto is best quality with affordable price and best
                        services for coustomer.
                    </p>
                    <button type="button" class="btn btn-primary button mt-md-5 mt-4"
                        onclick="window.location='about.php'">
                        <span>About Us</span>
                    </button>
                </div>
                <div class="col-lg-6 about-img text-lg-enter">
                    <img src="images/about.png" alt="" class="img-fluid">
                </div>
            </div>
        </div>


    </div>
    <!-- //banner-bottom-w3layouts -->
    <!-- <div class="some-another text-center pb-5">
        <div class="container pb-xl-5 pb-lg-3">
            <i class="fas fa-utensils"></i>
            <p class="text-uppercase mb-4 mt-2">A few words about our Site</p>
            <h4 class="aboutright about-right-2">Nemo enim ipsam voluptatem quia voluptas dolore magna aliqua
                Suspendisse
                interdum velit vel qu dapibus condimentum.</h4>
            <h5 class="text-uppsecase font-weight-bold text-dark mt-4">Chloe Jack
                <span class="text-secondary font-weight-normal">(Master Chef)</span>
            </h5>
        </div>
    </div> -->
    <!-- cake -->
    <!-- <img src="images/tiles.jpg" alt="" class="img-fluid cake-style"> -->

    <!-- //cake -->
</section>

<!-- services -->
<div class="serives-agile py-5 bg-light border-top" id="services">
    <div class="container py-xl-5 py-lg-3">
        <div class="row support-bottom text-center">
            <div class="col-md-4 support-grid">
                <i class="fas fa-eye"></i>
                <h5 class="text-dark text-uppercase mt-4 mb-3">Our Vision</h5>
                <p>To gain international recognization for our products through best quality, coust effectiveness,
                    leadership,innovation.</p>
            </div>
            <div class="col-md-4 support-grid my-md-0 my-4">
                <i class="far fa-chart-bar"></i>
                <h5 class="text-dark text-uppercase mt-4 mb-3">Our Mission</h5>
                <p>To become most innovative and valueable an barnd ammount ceramic world.</p>
            </div>
            <div class="col-md-4 support-grid">
                <i class="fas fa-asterisk"></i>
                <h5 class="text-dark text-uppercase mt-4 mb-3">Quaily assurance</h5>
                <p>Compnay is commited to provide best quality products being a quality assured firm,we have a special
                    team for quality inspections</p>
            </div>
        </div>
    </div>
</div>
<!-- //services -->

<!-- stats section -->
<div class="middlesection-agile ">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-lg-left text-center pt-4">
                <img src="images/i.jpg" style="margin-top: 73px; margin-left:-76px;" alt=""
                    class="img-fluid women-style" />
            </div>
            <div class="col-lg-6 left-build-wthree aboutright-agilewthree mt-0 py-5">
                <div class=" py-xl-5 py-lg-3">
                    <h2 class="title-2 text-white mb-sm-5 mb-4">Some Statistical Facts</h2>
                    <div class="row mb-xl-5 mb-4">
                        <div class="col-4 w3layouts_stats_left w3_counter_grid">
                            <p class="counter">50</p>
                            <p class="para-text-w3ls text-light">Products</p>
                        </div>
                        <div class="col-4 w3layouts_stats_left w3_counter_grid2">
                            <p class="counter">478</p>
                            <p class="para-text-w3ls text-light">Clients</p>
                        </div>
                        <div class="col-4 w3layouts_stats_left w3_counter_grid1">
                            <p class="counter">98.5</p>
                            <p class="para-text-w3ls text-light">Sercives(%)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //stats section -->

<!-- new products -->
<div class="featured-section py-5" id="products">
    <div class="container py-xl-5 py-lg-3">
        <div class="title text-center mb-5">
            <h3 class="text-dark mb-2">Our Products</h3>
        </div>
        <div class="content-bottom-in">
            <ul id="flexiselDemo1">
                <li>
                    <div class="w3l-specilamk">
                        <div class="row">
                            <!-- <div class="col-lg-6 product-name-w3l">
                                <h4 class="font-weight-bold">Strawberry Cakes</h4>
                                <p class="dont-inti-w3ls mt-4 mb-2">Sed ut perspiciatis unde omnis iste natus error sit
                                    voluptatem
                                    accuslaudantium.</p>
                                <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto vitae dicta sunt
                                    explicabo, Sed ut
                                    perspiciatis
                                    unde omnis iste natus error sit voluptatem accuslaudantium.</p>
                                <a href="#order" class="button-3 active mt-5 py-4 scroll">Order Now</a>
                            </div> -->
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/n3.jpg" alt="" class="img-fluid">
                                <center>
                                    <h4>Tiles</h4>
                                </center>
                            </div>
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/n4.jpg" alt="" class="img-fluid">
                                <center>
                                    <h4>Tiles</h4>
                                </center>

                            </div>
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/n5.jpg" alt="" class="img-fluid">
                                <center>
                                    <h4>Tiles</h4>
                                </center>

                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="w3l-specilamk">
                        <div class="row">
                            <!-- <div class="col-lg-6 product-name-w3l">
                                <h4 class="font-weight-bold">Strawberry Cakes</h4>
                                <p class="dont-inti-w3ls mt-4 mb-2">Sed ut perspiciatis unde omnis iste natus error sit
                                    voluptatem
                                    accuslaudantium.</p>
                                <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto vitae dicta sunt
                                    explicabo, Sed ut
                                    perspiciatis
                                    unde omnis iste natus error sit voluptatem accuslaudantium.</p>
                                <a href="#order" class="button-3 active mt-5 py-4 scroll">Order Now</a>
                            </div> -->
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/p3.jpg" alt="" class="img-fluid">
                                <center>
                                    <h4>Sanitaryware</h4>
                                </center>
                            </div>
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/p32.jpg" alt="" class="img-fluid">
                                <center>
                                    <h4>Sanitaryware</h4>
                                </center>
                            </div>
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/p33.jpg" alt="" class="img-fluid">
                                <center>
                                    <h4>Sanitaryware</h4>
                                </center>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="w3l-specilamk">
                        <div class="row">
                            <!-- <div class="col-lg-6 product-name-w3l">
                                <h4 class="font-weight-bold">Strawberry Cakes</h4>
                                <p class="dont-inti-w3ls mt-4 mb-2">Sed ut perspiciatis unde omnis iste natus error sit
                                    voluptatem
                                    accuslaudantium.</p>
                                <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto vitae dicta sunt
                                    explicabo, Sed ut
                                    perspiciatis
                                    unde omnis iste natus error sit voluptatem accuslaudantium.</p>
                                <a href="#order" class="button-3 active mt-5 py-4 scroll">Order Now</a>
                            </div> -->
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/s4.jpg" alt="" class="img-fluid">
                                <center>
                                    <h4>Bathware</h4>
                                </center>
                            </div>
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/s1.jpg" alt="" class="img-fluid">
                                <center>
                                    <h4>Bathware</h4>
                                </center>
                            </div>
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/s3.jpg" alt="" class="img-fluid">
                                <center>
                                    <h4>Bathware</h4>
                                </center>
                            </div>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="w3l-specilamk">
                        <div class="row">
                            <!-- <div class="col-lg-6 product-name-w3l">
                                <h4 class="font-weight-bold">Strawberry Cakes</h4>
                                <p class="dont-inti-w3ls mt-4 mb-2">Sed ut perspiciatis unde omnis iste natus error sit
                                    voluptatem
                                    accuslaudantium.</p>
                                <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto vitae dicta sunt
                                    explicabo, Sed ut
                                    perspiciatis
                                    unde omnis iste natus error sit voluptatem accuslaudantium.</p>
                                <a href="#order" class="button-3 active mt-5 py-4 scroll">Order Now</a>
                            </div> -->
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/toilet tub 1.jpg" alt="" class="img-fluid">
                                <center>
                                    <h4>Sanitaryware</h4>
                                </center>
                            </div>
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/toilet tub 2.jpg " alt="" class="img-fluid">
                                <center>
                                    <h4>Sanitaryware</h4>
                                </center>
                            </div>
                            <div class="col-lg-4 speioffer-agile">
                                <img src="images/toilet tub 3.jpg" alt="" class="img-fluid">
                                <center>
                                    <h4>Sanitaryware</h4>
                                </center>
                            </div>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
    </div>
</div>
<!-- //new products	-->

<!-- news -->
<!-- <div class="news-agile bg-cream py-5" id="news">
    <div class="container py-xl-5 py-lg-3">
        <div class="row">
            <div class="col-lg-4 order-form-w3ls pl-lg-0" id="order">
                <div class="agile_its_registration bg-white">
                    <h3 class="title-2 mb-3">Order a Cake</h3>
                    <p class="mb-4">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</p>
                    <form action="#" method="post">
                        <div class="agileits-location form-group">
                            <label>Shape</label>
                            <select required>
                                <option value="">Round</option>
                                <option value="1">rectangle</option>
                                <option value="2">square</option>
                                <option value="2">custom</option>
                            </select>
                            <div class="clear"></div>
                        </div>
                        <div class="agileits-location w3ls-1 form-group">
                            <label>Levels</label>
                            <select required>
                                <option value="">1</option>
                                <option value="1">2</option>
                                <option value="2">3</option>
                                <option value="2">4</option>
                            </select>
                            <div class="clear"></div>
                        </div>
                        <div class="agileits-location form-group">
                            <label>Size</label>
                            <select required>
                                <option value="">4 inch</option>
                                <option value="">6 inch</option>
                                <option value="">8 inch</option>
                                <option value="">10 inch</option>
                            </select>
                            <div class="clear"></div>
                        </div>
                        <div class="agileits-location w3ls-1 form-group">
                            <label>Flavor</label>
                            <select required>
                                <option value="">Chocolate</option>
                                <option value="">Butterscotch </option>
                                <option value="">Strawberry</option>
                                <option value="">Vanilla</option>
                            </select>
                            <div class="clear"></div>
                        </div>
                        <input type="submit" value="Order" class="agileinfo-order btn" />
                    </form>
                </div>
            </div>

            <div class="col-lg-8 news-blog mt-lg-0 mt-5">
                <h3 class="title-2 mb-md-5 mb-4">Company News</h3>
                <div class="row">
                    <div class="col-sm-6 news-grids-agile">
                        <div class="news-top">
                            <a href="single.html">
                                <img src="images/news1.jpg" alt="" class="img-fluid" />
                            </a>
                        </div>
                        <div class="price-bottom bg-white p-4">
                            <a href="single.html" class="text-dark">Sep 21st, 2018</a>
                            <h5 class="mt-3">Sit voluptatem accusantium doloremque</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 news-grids-agile mt-sm-0 mt-5">
                        <div class="news-top">
                            <a href="single.html">
                                <img src="images/news2.jpg" alt="" class="img-fluid" />
                            </a>
                        </div>
                        <div class="price-bottom bg-white p-4">
                            <a href="single.html" class="text-dark">Sep 21st, 2018</a>
                            <h5 class="mt-3">Sit voluptatem accusantium doloremque</h5>
                        </div>
                    </div>
                </div>
                <div class="faq-w3agile mt-5" id="faqs">
                    <h3 class="title-2 mb-md-5 mb-4">Frequently asked questions</h3>
                    <ul class="faq list-unstyled">
                        <li class="item1">
                            <a href="#" title="click here" class="text-dark border-bottom pb-3">Consectetur adipiscing
                                sit elitipsum nec?
                                <i class="fas fa-plus float-right"></i>
                            </a>
                            <ul class="list-unstyled">
                                <li class="subitem1">
                                    <p class="pt-3 pb-4"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                                        diam nonummy nibh euismod
                                        tincidunt ut laoreet dolore.
                                        At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                                        praesentium voluptatum deleniti
                                        atque
                                        corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non
                                        provident.</p>
                                </li>
                            </ul>
                        </li>
                        <li class="item2 my-3">
                            <a href="#" title="click here" class="text-dark border-bottom pb-3">The standard Lorem Ipsum
                                passage Etiam?
                                <i class="fas fa-plus float-right"></i>
                            </a>
                            <ul class="list-unstyled">
                                <li class="subitem1">
                                    <p class="pt-3 pb-4"> Tincidunt ut laoreet dolore At vero eos et Lorem ipsum dolor
                                        sit amet, consectetuer
                                        adipiscing elit, sed diam nonummy
                                        nibh euismod consectetuer adipiscing elit, sed diam nonummy nibh euismod
                                        accusamus et iusto odio dignissimos
                                        ducimus
                                        qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et
                                        quas molestias excepturi sint
                                        occaecati
                                        cupiditate non provident.</p>
                                </li>
                            </ul>
                        </li>
                        <li class="item3 my-3">
                            <a href="#" title="click here" class="text-dark border-bottom pb-3">Ut semper nisl ut
                                laoreet ultrices?
                                <i class="fas fa-plus float-right"></i>
                            </a>
                            <ul class="list-unstyled">
                                <li class="subitem1">
                                    <p class="pt-3 pb-4">Dincidunt ut laoreet dolore At vero eos et Lorem ipsum dolor
                                        sit amet, consectetuer
                                        adipiscing elit, sed diam nonummy
                                        nibh euismod consectetuer adipiscing elit, sed diam nonummy nibh euismod
                                        accusamus et iusto odio dignissimos
                                        ducimus
                                        qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et
                                        quas molestias excepturi sint
                                        occaecati
                                        cupiditate non provident.</p>
                                </li>
                            </ul>
                        </li>
                        <li class="item4">
                            <a href="#" title="click here" class="text-dark border-bottom pb-3">Sed diam nonummy nibh
                                eiusmod faucibus?
                                <i class="fas fa-plus float-right"></i>
                            </a>
                            <ul class="list-unstyled">
                                <li class="subitem1">
                                    <p class="pt-3">At vero eos et Lorem ipsum dolor sit amet, consectetuer adipiscing
                                        elit, sed diam nonummy
                                        nibh euismod consectetuer
                                        adipiscing elit, sed diam nonummy nibh euismod accusamus et iusto odio
                                        dignissimos ducimus qui blanditiis
                                        praesentium
                                        voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint
                                        occaecati cupiditate non
                                        provident.</p>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- //news -->

<!-- support -->
<!-- <div class="serives-agile py-5" id="support">
    <div class="container py-xl-5 py-lg-3">
        <div class="title text-center mb-5">
            <h3 class="text-dark mb-2">Help & Support</h3>
            <p>Ut enim ad minim veniam, quis nostrud ullam.</p>
        </div>
        <div class="row support-bottom text-center">
            <div class="col-md-4 support-grid">
                <i class="fas fa-headphones"></i>
                <h5 class="text-dark text-uppercase mt-4 mb-3">Online Support</h5>
                <p>Ut enim ad minima veniam, quis nostrum ullam corporis suscipit laboriosam.</p>
                <button type="button" class="button button-2" onclick="window.location='contact.html'">
                    <span>Call Us</span>
                </button>
            </div>
            <div class="col-md-4 support-grid my-md-0 my-5">
                <i class="far fa-comments"></i>
                <h5 class="text-dark text-uppercase mt-4 mb-3">Live Chat 24/7</h5>
                <p>Ut enim ad minima veniam, quis nostrum ullam corporis suscipit laboriosam.</p>
                <button type="button" class="button button-2 active" onclick="window.location='contact.html'">
                    <span>Let's Chart</span>
                </button>
            </div>
            <div class="col-md-4 support-grid">
                <i class="fas fa-question"></i>
                <h5 class="text-dark text-uppercase mt-4 mb-3">Any Questions</h5>
                <p>Ut enim ad minima veniam, quis nostrum ullam corporis suscipit laboriosam.</p>
                <button type="button" class="button button-2" onclick="window.location='contact.html'">
                    <span>Learn More</span>
                </button>
            </div>
        </div>
    </div>
</div> -->
<!-- support -->
<?php
include('include/footer.php');
?>