<?php
include('include/header.php');
?>
<!-- //header -->

<!-- banner 2 -->
<div class="banner2-w3ls">

</div>
<!-- //banner 2 -->
</div>
<!-- main -->
<!-- page details -->
<div class="breadcrumb-agile">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item">
                <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">About Us</li>
        </ol>
    </nav>
</div>
<!-- //page details -->

<!-- about page -->
<!-- about section --> <br><br>
<center>
    <h1 class="text-dark mb-2">About Us</h1>
</center>

<section class="bottom-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 about-img-pages">
            </div>
            <div class="col-lg-4 about-info">

                <div class="service-in text-left">
                    <div>
                        <div class="card-body" style="padding:0px">
                            <img src="images/about3.jpg" alt="">
                        </div>
                    </div>
                </div><br><br><br>
                <div class="service-in text-left">
                    <div class="">
                        <div class="card-body" style="padding:0px">
                            <img src="images/about4.jpg" alt="">
                        </div>
                    </div>
                </div> <br><br><br>
                <div class="service-in text-left">
                    <div class="">
                        <div class="card-body" style="padding:0px">
                            <img src="images/about6.jpg" alt="">
                        </div>
                    </div>
                </div><br><br><br><br><br>
            </div>
        </div>
    </div>
</section>

<section class="bottom-banner">
    <div class="container-fluid"    >
        <div class="row">
            <div class="col-lg-6 about-img-pagesss">
            </div>
            <div class="col-lg-6 about-img-pagess">
            </div>
        </div>
    </div>
</section>
<!-- //about section -->

<!-- about bottom -->
<!-- <div class="some-another some-another-page text-center py-5">
    <div class="container py-xl-5 py-lg-3">
        <i class="fas fa-utensils"></i>
        <p class="text-uppercase mb-4 mt-2">A few words about our Site</p>
        <h4 class="aboutright about-right-2">Nemo enim ipsam voluptatem quia voluptas dolore magna aliqua Suspendisse
            interdum velit vel qu dapibus condimentum.</h4>
        <h5 class="text-uppsecase font-weight-bold text-dark mt-4">Chloe Jack
            <span class="text-secondary font-weight-normal">(Master Chef)</span>
        </h5>
    </div>
    <img src="images/product1.jpg" alt="" class="img-fluid w3ls-img-styles">
</div> -->
<!-- //about bottom -->

<!-- services -->
<div class="serives-agile py-5 bg-light border-top" id="services">
    <div class="container py-xl-5 py-lg-3">
        <div class="row support-bottom text-center">
            <div class="col-md-4 support-grid">
                <i class="fas fa-check-square"></i>
                <h5 class="text-dark text-uppercase mt-4 mb-3">TRUST</h5>
                <p>In people and work is valuable and vital for us to build and deliver on our shared vision.</p>
            </div>
            <div class="col-md-4 support-grid my-md-0 my-4">
                <i class="fas fa-bolt"></i>
                <h5 class="text-dark text-uppercase mt-4 mb-3">INNOVATION</h5>
                <p>To seek new ways to harness the benefits of technology and yet to be conscious of the environment
                    every day.</p>
            </div>
            <div class="col-md-4 support-grid">
                <i class="fas fa-users"></i>
                <h5 class="text-dark text-uppercase mt-4 mb-3">24 * 7</h5>
                <p> This support generally
                    includes support for those services that require running without disruption and downtime.</p>
            </div>
        </div>
    </div>
</div>
<!-- //services -->

<!-- stats section -->
<!-- <div class="middlesection-agile ">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-lg-left text-center pt-4">
                <img src="images/women.png" alt="" class="img-fluid women-style" />
            </div>
            <div class="col-lg-6 left-build-wthree aboutright-agilewthree mt-0 py-5">
                <div class=" py-xl-5 py-lg-3">
                    <h2 class="title-2 text-white mb-sm-5 mb-4">Some Statistical Facts</h2>
                    <div class="row mb-xl-5 mb-4">
                        <div class="col-4 w3layouts_stats_left w3_counter_grid">
                            <p class="counter">1680</p>
                            <p class="para-text-w3ls text-light">Popularity</p>
                        </div>
                        <div class="col-4 w3layouts_stats_left w3_counter_grid2">
                            <p class="counter">1200</p>
                            <p class="para-text-w3ls text-light">Happy Customers</p>
                        </div>
                        <div class="col-4 w3layouts_stats_left w3_counter_grid1">
                            <p class="counter">400</p>
                            <p class="para-text-w3ls text-light">Awards Won</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="featured-section py-5" id="products">
    <div class="container py-xl-5 py-lg-3">
        <div class="title text-center mb-5">
            <h3 class="text-dark mb-2">Our Products</h3>
        </div>
        <div class="content-bottom-in">
            <ul id="flexiselDemo1">
                <li>
                    <div class="w3l-specilamk">
                        <div class="row">
                            <div class="col-lg-6 product-name-w3l">
                                <h4 class="font-weight-bold">Tiles Ware</h4>
                                <p class="dont-inti-w3ls mt-4 mb-2">Tiles Accessories</p>
                                <p>These belong to the class of ceramics and are called as white wares. Tiles that are
                                    used on walls and floor are available in both glazed and in the unglazed forms.</p>
                            </div>
                            <div class="col-lg-6 speioffer-agile">
                                <img src="images/tilesware.jpg" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="w3l-specilamk">
                        <div class="row">
                            <div class="col-lg-6 product-name-w3l">
                                <h4 class="font-weight-bold">Sanitary Ware</h4>
                                <p class="dont-inti-w3ls mt-4 mb-2">Sanitary Accessories</p>
                                <p> Top quality Sanitaryware closet and basin design’s with constant
                                    adaptation to new market trends. Our products are the result of a manufacturing
                                    process that places them at the very forefront of world technology. Respectful
                                    towards the environment (minimizes the use of water and energy), first company to
                                    develop products to flush in 4 litre saves almost 33% water per flush.
                                </p>
                            </div>
                            <div class="col-lg-6 speioffer-agile">
                                <img src="images/sanitary.jpeg" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="w3l-specilamk">
                        <div class="row">
                            <div class="col-lg-6 product-name-w3l">
                                <h4 class="font-weight-bold">Bath ware</h4>
                                <p class="dont-inti-w3ls mt-4 mb-2">Bathrooms Accessories</p>
                                <p>High Quality Bathrooms Accessories with Competitive Price. Quality China Products.
                                    SGS Audited Suppliers. Leading B2B Portal. China’s B2B Impact Award. Types:
                                    Construction, Metallurgy, Mineral, Chemicals, Security & Protection.</p>
                            </div>
                            <div class="col-lg-6 speioffer-agile">
                                <img src="images/bath.jpg" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- //stats section -->

<!-- team -->
<!-- <div class="team py-5" id="team">
    <div class="container py-xl-5 py-lg-3">
        <div class="title text-center mb-5">
            <h3 class="text-dark mb-2">Our Team</h3>
            <p>Ut enim ad minim veniam, quis nostrud ullam.</p>
        </div>
        <div class="row support-bottom text-center"> -->
<!-- Hover Effect Style : Demo - 15 -->
<!-- <div class="container">
                <div class="row mt-30">
                    <div class="col-md-3 col-6">
                        <div class="box15">
                            <img src="images/t1.jpg" alt="" class="img-fluid">
                            <div class="box-content">
                                <h3 class="title">Cruiser</h3>
                                <ul class="icon">
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="box15">
                            <img src="images/t2.jpg" alt="" class="img-fluid">
                            <div class="box-content">
                                <h3 class="title">Sthesia</h3>
                                <ul class="icon">
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="box15">
                            <img src="images/t3.jpg" alt="" class="img-fluid">
                            <div class="box-content">
                                <h3 class="title">Frapples</h3>
                                <ul class="icon">
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="box15">
                            <img src="images/t4.jpg" alt="" class="img-fluid">
                            <div class="box-content">
                                <h3 class="title">Molive</h3>
                                <ul class="icon">
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
<!-- </div>
    </div>
</div> -->
<!-- team -->

<?php
include('include/footer.php');
?>